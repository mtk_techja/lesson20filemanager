package com.klm.lesson20filemanager;

import android.Manifest;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        //loadFileDB();
        loadAudio();
    }

    private void loadAudio() {
        //Buoc1
        Uri dbPath = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        //B2
        String[] selectColumns = new String[]{
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.SIZE,
                MediaStore.Audio.Media.COMPOSER,
                MediaStore.Audio.Media.TRACK,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.DATE_MODIFIED,
        };
        Cursor c = getContentResolver().query(dbPath, selectColumns, null, null, null);
        if (c == null) return;
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String id = getCData(c, MediaStore.Audio.Media._ID);
            String data = getCData(c, MediaStore.Audio.Media.DATA);
            String title = getCData(c, MediaStore.Audio.Media.TITLE);
            String size = getCData(c, MediaStore.Audio.Media.SIZE);
            String compose = getCData(c, MediaStore.Audio.Media.COMPOSER);
            String track = getCData(c, MediaStore.Audio.Media.TRACK);
            String artist = getCData(c, MediaStore.Audio.Media.ARTIST);
            String album = getCData(c, MediaStore.Audio.Media.ALBUM);
            String dateModified = getCData(c, MediaStore.Audio.Media.DATE_MODIFIED);

            Log.i(TAG, "Id: " + id +
                    "\nData: " + data +
                    "\ntitle: " + title +
                    "\nSize: " + size +
                    "\ncompose: " + compose +
                    "\ntrack: " + track +
                    "\nartist: " + artist +
                    "\nalbum: " + album +
                    "\ndateModified" + dateModified);
            c.moveToNext();
        }
        c.close();
    }

    private void loadFileDB() {
        //Buoc1
        Uri dbPath = MediaStore.Files.getContentUri("external");

        //B2
        String[] selectColumns = new String[]{
                MediaStore.Files.FileColumns._ID,
                MediaStore.Files.FileColumns.DISPLAY_NAME,
                MediaStore.Files.FileColumns.TITLE,
                MediaStore.Files.FileColumns.SIZE,
                MediaStore.Files.FileColumns.DATA,
                MediaStore.Files.FileColumns.DATE_MODIFIED,
                MediaStore.Files.FileColumns.PARENT,
                MediaStore.Files.FileColumns.MEDIA_TYPE,
        };
        Cursor c = getContentResolver().query(dbPath, selectColumns, null, null, null);
        if (c == null) return;
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String id = getCData(c, MediaStore.Files.FileColumns._ID);
            String disPlayname = getCData(c, MediaStore.Files.FileColumns.DISPLAY_NAME);
            String title = getCData(c, MediaStore.Files.FileColumns.TITLE);
            String parent = getCData(c, MediaStore.Files.FileColumns.PARENT);
            String date = getCData(c, MediaStore.Files.FileColumns.DATE_MODIFIED);
            String path = getCData(c, MediaStore.Files.FileColumns.DATA);
            String size = getCData(c, MediaStore.Files.FileColumns.SIZE);
            String mediaType = getCData(c, MediaStore.Files.FileColumns.MEDIA_TYPE);

            Log.i(TAG, "Id: " + id +
                    "\nDisplay: " + disPlayname +
                    "\ntitle: " + title +
                    "\nparent: " + parent +
                    "\nmediaType: " + mediaType +
                    "\nData: " + path +
                    "\nSize: " + size +
                    "\nDateModified: " + date);
            c.moveToNext();
        }
        c.close();
    }

    private String getCData(Cursor c, String name) {
        return c.getString(c.getColumnIndex(name));
    }
}
